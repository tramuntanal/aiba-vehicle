#define ENABLE_R3_SHIELD_MOTOR_DRIVER
#define ENABLE_NEWPING_DISTANCE_SENSOR_DRIVER

//
// Motors
//
#ifdef ENABLE_R3_SHIELD_MOTOR_DRIVER
#include "r3_motor_shield_motor_driver.h"

#define MOTOR_R_DIRECTION_PIN 12
#define MOTOR_R_BRAKE_PIN     9
#define MOTOR_R_SPEED_PIN     3
AiBa::ArduinoR3Motor motorR(MOTOR_R_DIRECTION_PIN, MOTOR_R_BRAKE_PIN, MOTOR_R_SPEED_PIN);

#define MOTOR_L_DIRECTION_PIN 13
#define MOTOR_L_BRAKE_PIN     8
#define MOTOR_L_SPEED_PIN     11
AiBa::ArduinoR3Motor motorL(MOTOR_L_DIRECTION_PIN, MOTOR_L_BRAKE_PIN, MOTOR_L_SPEED_PIN);
#endif

//
// Sensors
//
#define TOO_CLOSE 15
#define MAX_DISTANCE (TOO_CLOSE * 20)

#ifdef ENABLE_NEWPING_DISTANCE_SENSOR_DRIVER
#include <NewPing.h>
#include "newping_distance_sensor_driver.h"
#define DIST_SENSOR_ECHO_PIN 5
#define DIST_SENSOR_TRIGGER_PIN 4
#define DISTANCE_SENSOR_INIT DIST_SENSOR_TRIGGER_PIN, DIST_SENSOR_ECHO_PIN, MAX_DISTANCE
#endif
#include "moving_average.h"

//
// Logging
//
#define LOGGING
#include "logging.h"

namespace AiBa
{
    class Vehicle
    {
    public:
        /*
         * @brief Class constructor.
         */
        Vehicle(MotorDriver &rMotor, MotorDriver &lMotor)
            : rightMotor(rMotor), leftMotor(lMotor),
              distanceSensor(DISTANCE_SENSOR_INIT),
              distanceAverage(MAX_DISTANCE)
        {
            initialize();
        }

        /*
         * @brief Initialize the robot state.
         */
        void initialize()
        {
            moveFw();
            state = stateRunning;
        }
        /*
         * @brief Update the state of the robot based on input from sensor and remote control.
         *  Must be called repeatedly while the robot is in operation.
         */
        void run()
        {
          log("state: %u ", state);
          int distance = distanceAverage.add(distanceSensor.getDistance());
          log("AVG distance: %ucm\n", distance);
          if (state == stateRunning) {
              if (hasObstacle(distance)) {
                  state = stateStopped;
                  stopMoving();
              } else {
                  state = stateRunning;
                  moveFw();
              }
          } else if (state == stateStopped) {
            if (hasObstacle(distance)) {
              log("OBSTACLE\n");
              // the vehicle only stops running because it has found an obstacle, in this case, turn to some other direction and run again
              int dir= rand() % 2;
              int duration= rand() % 1000; // turn during at max 1second
              if (dir == 0) { turnRight(duration); }
              else { turnLeft(duration); }
            } else {
              log("NO obstacle\n");
              // the vehicle turned and there's no obstacle any more, run again!
              state= stateRunning;
              moveFw();
            }
          }
        }

        bool hasObstacle(int distance) {
          return distance <= TOO_CLOSE;
        }
        void moveFw() {
          leftMotor.setSpeed(255);
          rightMotor.setSpeed(255);
        }
        void stopMoving() {
          leftMotor.setSpeed(-10);
          rightMotor.setSpeed(-10);
          delay(500);
          leftMotor.setSpeed(1);
          rightMotor.setSpeed(1);
          delay(250);
          leftMotor.setSpeed(0);
          rightMotor.setSpeed(0);
        }
        void turnRight(int duration) {
          leftMotor.setSpeed(255);
          rightMotor.setSpeed(-255);
          delay(duration);
          leftMotor.setSpeed(0);
          rightMotor.setSpeed(0);
        }
        void turnLeft(int duration) {
          leftMotor.setSpeed(-255);
          rightMotor.setSpeed(255);
          delay(duration);
          leftMotor.setSpeed(0);
          rightMotor.setSpeed(0);
        }

    private:
        MotorDriver &rightMotor;
        MotorDriver &leftMotor;
        DistanceSensor distanceSensor;
        enum state_t { stateStopped, stateRunning };
        state_t state;
        MovingAverage<unsigned int, 3> distanceAverage;
    };
};



AiBa::Vehicle vehicle(motorR, motorL);


void setup() {
  Serial.begin(9600);
  vehicle.initialize();
}


void loop() {
//motor1.setSpeed(255);
//motor2.setSpeed(255);
//delay(2000);
//
//motor1.setSpeed(0);
//motor2.setSpeed(0);
//delay(1000);
//
//motor1.setSpeed(-255);
//motor2.setSpeed(-255);
//delay(2000);


  vehicle.run();
}
