/**
 * @file r3_motor_shield_driver.h
 * @brief Motor device driver for the Arduino R3 motor shield.
 * @author Oliver Valls
 */

 #include "motor_driver.h"

namespace AiBa
{
  class ArduinoR3Motor : public MotorDriver
    {
    public:
        /*
         * @brief Class constructor.
         * @param number the DC motor number to control, from 1 to 4.
         */
        ArduinoR3Motor(int dirP, int brakeP, int speedP)
            : MotorDriver(), directionPin(dirP), brakePin(brakeP), speedPin(speedP), currentSpeed(0)
        {
        }

        void setSpeed(int speed)
        {
            currentSpeed = speed;
            if (speed == 0) {
              digitalWrite(brakePin, HIGH);
              analogWrite(speedPin, 0);
            } else {
              digitalWrite(brakePin, LOW);
              analogWrite(speedPin, abs(speed));
              if (speed > 0) {
                digitalWrite(directionPin, HIGH);
              } else if (speed < 0) {
                digitalWrite(directionPin, LOW);
              }
            }
        }

        int getSpeed() const
        {
            return currentSpeed;
        }

    private:
        int directionPin;
        int brakePin;
        int speedPin;
        int currentSpeed;
    };
};
